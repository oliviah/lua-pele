﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Character : MonoBehaviour {

	public float force;
	private Vector3 moveDirection = Vector3.zero;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void FixedUpdate () {
		if (Input.GetKey ("w")) {
			transform.position += Vector3.up * force * Time.deltaTime;
		}
		if (Input.GetKey ("s")) {
			transform.position += Vector3.down * force * Time.deltaTime;
		}
		if (Input.GetKey ("a")) {
			transform.rotation = Quaternion.Euler (0, -90, 0);
		}
		if (Input.GetKey ("d")) {
			transform.rotation = Quaternion.Euler (0, 90, 0);
		}
	}
}
