﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WorldRotate : MonoBehaviour {

	public float rotation;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void FixedUpdate () {
		if (Input.GetKey ("a")) {
			transform.Rotate (0, -3 * Time.deltaTime, 0);
		}
		if (Input.GetKey ("d")) {
			transform.Rotate (0, rotation * Time.deltaTime, 0);
		}
	}
}
