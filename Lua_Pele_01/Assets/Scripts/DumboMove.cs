﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DumboMove : MonoBehaviour {

	public float DumboForce;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void FixedUpdate () {
		if (Input.GetKey (KeyCode.LeftArrow)) {
			transform.position += Vector3.left * DumboForce * Time.deltaTime;
		}
		if (Input.GetKey (KeyCode.RightArrow)) {
			transform.position += Vector3.right * DumboForce * Time.deltaTime;
		}
		if (Input.GetKey (KeyCode.UpArrow)) {
			transform.position += Vector3.up * DumboForce * Time.deltaTime;
		}
		if (Input.GetKey (KeyCode.DownArrow)) {
			transform.position += Vector3.down * DumboForce * Time.deltaTime;
		}
	}
		
}
